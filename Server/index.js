
'use strict' // a decisión de  uno

//llamar las librerías de body parser y express
const bodyParser = require('body-parser');
const express = require('express');

//iniciamos express
const app = express();

// Activar los cors
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next(); //ejecuta
});

// Activar middlewares de express esto transformará los datos al tipo de bd que estamos utilizando
app.use(bodyParser.urlencoded({ extended : false}));
app.use(bodyParser.json());

app.use(require('../Routes/routes')); //no hay problema con el nombre del archivo js de la carpeta routes




module.exports = app; //en esta sí van porque si no no jala