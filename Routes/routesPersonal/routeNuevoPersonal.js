const express = require('express');
const modelPersonalNuevo = require('../../Models/Personal/modelNuevoPersonal');

let app = express();

app.post('/personal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaPersonal = new modelPersonalNuevo ({
        nombrePersonal: body.nombrePersonal,
        apellidopaPersonal: body.apellidopaPersonal,
        apellidomaPersonal: body.apellidomaPersonal,
        telefonoPersonal: body.telefonoPersonal,
        statusPersonal: body.statusPersonal,
    });
    newSchemaPersonal
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados amix',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Hubo problemillas',
                        err
                    });
            }
        );
            
});

//Obtener
app.get('/obtener/personal', async (_req,res)=>{
    const respuesta = await modelPersonalNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});
//Actualizar
app.put('/update/personal/:id', async (req,res)=>{
let id= req.params.id;
let campos = req.body;

const respuesta = await modelPersonalNuevo.findByIdAndUpdate(id,campos,{new:true});
res.status(202).json({
    ok:true,
    msj:"CAMPOS ACTUALIZADOS CORRECTAMENTE",
    respuesta
});
});

//Eliminar
app.delete('/delete/personal/:id', async (req,res)=>{
let id = req.params.id;
const respuesta = await modelPersonalNuevo.findByIdAndDelete(id);
res.status(200).json({
    ok:true,
    msj:"EL REGISTRO SE ELIMINO",
    respuesta
});
});

module.exports = app;