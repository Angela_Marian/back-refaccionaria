const express = require('express');
const modelSucursalNueva = require('../../Models/Sucursales/modelNuevasSucursales');

let app = express();

app.post('/sucursal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaSucursal = new modelSucursalNueva({
        nombreSucursal: body.nombreSucursal,
        paisSucursal: body.paisSucursal,
        direccionSucursal: body.direccionSucursal,
        telefonoSucursal: body.telefonoSucursal,
        statusSucursal: body.statusSucursal,
    });
    newSchemaSucursal
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados amix',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Hubo problemillas',
                        err
                    });
            }
        );
            
});

//Obtener
app.get('/obtener/sucursal', async (_req,res)=>{
    const respuesta = await modelSucursalNueva.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});
//Actualizar
app.put('/update/sucursal/:id', async (req,res)=>{
let id= req.params.id;
let campos = req.body;

const respuesta = await modelSucursalNueva.findByIdAndUpdate(id,campos,{new:true});
res.status(202).json({
    ok:true,
    msj:"CAMPOS ACTUALIZADOS CORRECTAMENTE",
    respuesta
});
});

//Eliminar
app.delete('/delete/sucursal/:id', async (req,res)=>{
let id = req.params.id;
const respuesta = await modelSucursalNueva.findByIdAndDelete(id);
res.status(200).json({
    ok:true,
    msj:"EL REGSISTRO SE ELIMINO",
    respuesta
});
});

module.exports = app;