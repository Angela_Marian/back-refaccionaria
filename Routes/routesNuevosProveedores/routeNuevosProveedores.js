const express = require('express');
const modelProveedorNuevo = require('../../Models/Proveedores/modelNuevosProveedores');

let app = express();

app.post('/proveedor/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProveedor = new modelProveedorNuevo ({
        marcaProveedor: body.marcaProveedor,
        localizacionProveedor: body.localizacionProveedor,
        telefonoProveedor: body.telefonoProveedor,
        statusProveedor: body.statusProveedor, 
        descripcionProveedor: body.descripcionProveedor,
    });
    newSchemaProveedor
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados amix',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Hubo problemillas',
                        err
                    });
            }
        );
            
});
//Obtener
app.get('/obtener/proveedor', async (_req,res)=>{
    const respuesta = await modelProveedorNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});
//Actualizar
app.put('/update/proveedor/:id', async (req,res)=>{
let id= req.params.id;
let campos = req.body;
const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id,campos,{new:true});
res.status(202).json({
    ok:true,
    msj:"CAMPOS ACTUALIZADOS CORRECTAMENTE",
    respuesta
});
});

//Eliminar
app.delete('/delete/proveedor/:id', async (req,res)=>{
let id = req.params.id;
const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);
res.status(200).json({
    ok:true,
    msj:"EL REGISTRO SE ELIMINO",
    respuesta
});
});

module.exports = app;