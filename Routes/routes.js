const express = require('express');
const app = express();


app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./routesNuevosProveedores/routeNuevosProveedores'));
app.use(require('./routesPersonal/routeNuevoPersonal'));
app.use(require('./routesNuevasSucursales/routeNuevasSucursales'));

module.exports = app;