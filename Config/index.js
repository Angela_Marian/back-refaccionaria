'use strict'

const mongoose = require('mongoose');
const app = require('../Server/index');
const port = 3900; //numero de puerto donde vamos a correr el servidor

// Generar una promesa global
mongoose.Promise = global.Promise; //que va a generar ciertas cosas mongoose

//Hacer la conexion a la db
mongoose.connect('mongodb://127.0.0.1:27017/refaccionaria', { useNewUrlParser: true})
    .then(() => {
        console.log('Base de datos corriendo amix')

        //Escuchar el puerto del server
        app.listen(port, () => { //como app está vaío hay un error, no recuerda el profe para qué sirve el parser
            console.log(`Server corriendo en puerto: ${port}`);
        });
    })