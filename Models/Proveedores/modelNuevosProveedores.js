const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let nuevoProveedorRefaccionaria = new Schema({
    marcaProveedor: { type: String },
    localizacionProveedor: { type: String },
    telefonoProveedor: { type: Number },
    statusProveedor: { type: Boolean },
    descripcionProveedor: { type: String },
});

module.exports = mongoose.model('nuevoProveedor', nuevoProveedorRefaccionaria);