const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let nuevaSucursalRefaccionaria = new Schema({
    nombreSucursal: { type: String },
    paisSucursal: { type: String },
    direccionSucursal: { type: String },
    telefonoSucursal: { type: Number },
    statusSucursal: { type: Boolean },
    
});

module.exports = mongoose.model('nuevaSucursal', nuevaSucursalRefaccionaria);