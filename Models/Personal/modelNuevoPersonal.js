const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let nuevoPersonalRefaccionaria = new Schema({
    nombrePersonal: { type: String },
    apellidopaPersonal: { type: String },
    apellidomaPersonal: { type: String },
    telefonoPersonal: { type: Number },
    statusPersonal: { type: Boolean },
    
});

module.exports = mongoose.model('nuevoPersonal', nuevoPersonalRefaccionaria);